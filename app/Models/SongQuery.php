<?php namespace App\Models;


class SongQuery {

  public function search(array $searchParams)
  {
    $query = \DB::table('songs')
      ->join('artists', 'songs.artist_id', '=', 'artists.id')
      ->join('genres', 'songs.genre_id', '=', 'genres.id')
      ->where('title', 'LIKE', "%" . $searchParams['title'] . "%")
      ->orderBy('artist_name', 'asc');

    return $query->get();
  }

} 