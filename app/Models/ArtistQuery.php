<?php namespace App\Models; 

use DB;

class ArtistQuery {

	public function count()
	{
		return DB::table('artists')->count();
	}


	public static function create()
	{
		return new ArtistQuery();
	}

}