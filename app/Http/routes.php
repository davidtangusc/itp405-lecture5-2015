<?php

Route::get('/', 'SongsController@search');
Route::get('/songs/search', 'SongsController@search');
Route::get('/songs', 'SongsController@results');