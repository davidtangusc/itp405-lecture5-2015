<?php namespace App\Http\Controllers;

use App\Models\SongQuery;
use App\Models\ArtistQuery;
use Illuminate\Http\Request;

class SongsController extends Controller {

  public function search()
  {
    return view('search', [
      'artistCount' => ArtistQuery::create()->count()
    ]);
  }


  public function results(Request $request)
  {
    $title = $request->input('song_title');

    if (!$title) {
      return redirect('/songs/search');
    }

    return view('results', [
        'songs' => (new SongQuery())->search([ 'title' => $title ]),
        'searchTerm' => $title
    ]);
  }

} 