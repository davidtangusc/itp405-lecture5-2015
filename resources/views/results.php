<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

<h1>Results</h1>

<?php //dd($songs) ?>

<p>You searched for: <strong><?php echo $searchTerm ?></strong></p>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Artist</th>
      <th>Title</th>
      <th>Genre</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($songs as $song) : ?>
    <tr>
      <td><?php echo $song->artist_name ?></td>
      <td><?php echo $song->title ?></td>
      <td><?php echo $song->genre ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>


</body>
</html>